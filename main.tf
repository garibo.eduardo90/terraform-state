provider "aws" {
  region = "us-east-1"
}

variable "default_subnet_cidr_block" {
  description = "Cidr block for the subnet in the default vpc"
}

variable "vpc_cidr_block" {}
variable "env_prefix" {}
variable "subnet_cidr_block" {
  description = "Cidr block for the subnet"
}
variable "instance_type" {}
data "aws_region" "current" {}
variable "public_key_location" {}
resource "aws_vpc" "main-vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "main_subnet_1" {
  cidr_block        = var.subnet_cidr_block
  vpc_id            = aws_vpc.main-vpc.id
  availability_zone = "${data.aws_region.current.name}a"

  tags = {
    Name = "${var.env_prefix}-subnet-1",
    ManagedBy : "Terraform"
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "development-subnet-2" {
  cidr_block        = var.default_subnet_cidr_block
  vpc_id            = data.aws_vpc.existing_vpc.id
  availability_zone = "${data.aws_region.current.name}b"

  tags = {
    Name = "${var.env_prefix}-subnet-2",
    ManagedBy : "Terraform"
  }
}

resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.main-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_igw.id
  }

  tags = {
    Name = "${var.env_prefix}-rt",
    ManagedBy : "Terraform"
  }
}

resource "aws_route_table_association" "subnet1_association" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id      = aws_subnet.main_subnet_1.id
}

resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main-vpc.id

  tags = {
    Name : "${var.env_prefix}-rt",
    ManagedBy : "Terraform"
  }
}

resource "aws_security_group" "app-sg" {
  name   = "app-security-group"
  vpc_id = aws_vpc.main-vpc.id

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
  }
  ingress {
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux" {
  most_recent = true
  owners      = [
    "amazon"]
  filter {
    name   = "name"
    values = [
      "amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = [
      "hvm"]
  }
}

resource "aws_key_pair" "server-key" {
  key_name   = "server_key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "app-server" {
  ami           = data.aws_ami.latest-amazon-linux.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.main_subnet_1.id

  vpc_security_group_ids = [
    aws_security_group.app-sg.id]

  availability_zone           = "${data.aws_region.current.name}a"
  associate_public_ip_address = true
  key_name                    = aws_key_pair.server-key.key_name

  user_data = file("entryScript.sh")

  tags = {
    Name      = "${var.env_prefix}-server-instance",
    ManagedBy = "Terraform"
  }
}
